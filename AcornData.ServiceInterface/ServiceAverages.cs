﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornData.Entities;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using System.Globalization;
using ServiceStack.ServiceHost;
namespace AcornData.ServiceInterface
{
    public class ServiceAverages : RestServiceBase<tAverageRequest>
    {
        public override object OnGet(tAverageRequest request)
        {
            if (request.RequestType.Equals("Rainfall", StringComparison.CurrentCultureIgnoreCase))
                return GetAverageRainfallBetweenRangeByStation(request);
            if (request.RequestType.Equals("Temp", StringComparison.CurrentCultureIgnoreCase))
                return GetAverageTempsBetweenRangeByStation(request);

            throw new HttpError(System.Net.HttpStatusCode.NotFound, "We don't know how to handle a request for " + request.RequestType);
        }


        private object GetAverageTempsBetweenRangeByStation(tAverageRequest request)
        {
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var result = (from temps in entities.spGetAverageTempsByDateRange(request.StationID, request.sDate, request.eDate)
                              select temps).First();
                return result;
            }
        }

        private object GetAverageRainfallBetweenRangeByStation(tAverageRequest request)
        {
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var result = (from rainfall in entities.spGetAverageRainfallByDateRange(request.StationID, request.sDate, request.eDate)
                              select rainfall).First();
                return result;
            }
        }


    }
}

