﻿using System;
using System.Net;
using System.IO.IsolatedStorage;
using System.IO;

namespace ConsoleApplication1
{
    public class CookieAwareWebClient : WebClient
    {
        [System.Security.SecuritySafeCritical]
        public CookieAwareWebClient()
            : base()
        {

        }

        private static CookieAwareWebClient singletonWebClient;

        public static CookieAwareWebClient SingletonWebClient
        {
            get
            {
                if (singletonWebClient == null)
                {
                    singletonWebClient = new CookieAwareWebClient();
                }
                return singletonWebClient;
            }
        }

        public CookieContainer CookieJar = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                if (CookieJar == null)
                    CookieJar = new CookieContainer();

                (request as HttpWebRequest).CookieContainer = CookieJar;
            }
            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
        {
            WebResponse response = base.GetWebResponse(request, result);
            if (response is HttpWebResponse)
            {
                if (CookieJar == null)
                    CookieJar = new CookieContainer();
                CookieJar.Add(request.RequestUri, (response as HttpWebResponse).Cookies);
            }

            return response;
        }
    }
}