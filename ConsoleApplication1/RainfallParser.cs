﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornData.Entities;
using AcornData.Entities.NonDatabaseEntities;

namespace ConsoleApplication1
{
    public class RainfallParser
    {

        public static List<RainfallSpecific> GetRainfallForStationAndDate(string csvFile)
        {
            List<RainfallSpecific> result = new List<RainfallSpecific>();

            var comparer = new GenericEqualityComparer<RainfallSpecific>(o => o.Date);

            foreach (string row in (csvFile.Trim().Split(new string[] { "\r\n" }, StringSplitOptions.None).Skip(1))) //probably shouldn't skip the first
            {
                RainfallSpecific rain = new RainfallSpecific();
                string[] cols = row.Split(',');

                rain.StationID = cols[1];
                rain.Date = cols[2] + cols[3] + cols[4];
                if(cols[5].Length > 0)
                rain.RainfallAmount = double.Parse(cols[5]);
                rain.Quality = cols[7] == "Y";

                result.Add(rain);

            }

            return result;
        }
    }
}
