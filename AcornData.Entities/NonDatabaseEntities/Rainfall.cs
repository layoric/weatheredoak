﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcornData.Entities.NonDatabaseEntities
{
    public class RainfallSpecific
    {
        public string StationID { get; set; }
        public string Date { get; set; }
        public double? RainfallAmount { get; set; }
        public bool Quality { get; set; }
    }
}
